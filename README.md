# Gravekeeper

## Status             
[![Build status](https://gitlab.com/shpak.mariya.ed/gravekeeper/badges/master/pipeline.svg)](https://gitlab.com/shpak.mariya.ed/gravekeeper/pipelines)

## About
Gravekeeper - 2D single-player game based on my own micro engine (using SDL2 + OpenGL). 
Demo version was developed on the basis of the [course](https://www.it-academy.by/course/c-game-developer/game-developer/). For more information how I came to this result, you can look at the [repository](https://gitlab.com/shpak.mariya.ed/gamedev_learning).
I will be grateful for your ideas and help in this project development! :)

## Screenshots
![start_menu](screenshots/start_menu.png)
![game_process_0](screenshots/game_process_0.png)
![game_process_1](screenshots/game_process_1.png)

## How to get
If you use IDE, select as a working directory **game** folder.
Using terminal (from repo root):

    cd game/
    cmake -S . -B game/build/
    cmake --build build/
    ./build/game 

## TODO
### Firstly:
- add Android touch control
- add decision tree (for enemies only)
- add pause
- fix retry method (after win/lose)
- refactor game objects class system
- refactor menu design

### Secondary:
- add new enemies
- block staying in the exit point possibility (to win)
- add smarter collision
- add ImGUI into the game
- etc
   
## Software stack
Technologies which I use for development:
- Ubuntu 20.04.0
- GCC 9.3.0
- CMake 3.17.0+
- SDL2 2.0.12+

## Docker
You can look at the docker image I use [on Docker Hub](https://hub.docker.com/r/shpakmariya/gamedev_learning/) or just pull that:

    docker pull shpakmariya/gamedev_learning