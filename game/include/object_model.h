#pragma once

#include "engine.h"

namespace game {
using vao_buffer = om::triangle_buffer<om::triangle_textured>;
using vao_buffer_colored = om::triangle_buffer<om::triangle_colored>;
using texture_ptr = om::texture *;

struct object_model {
    vao_buffer vao;
    float width = 0.15f;
    texture_ptr texture;
};
} // namespace game