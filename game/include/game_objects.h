#pragma once

#include <array>
#include <chrono>

#include "animation_2d.h"
#include "object_model.h"
#include "timer.h"

namespace game {
enum class obj_type { passive, active };
enum class obj_role { main_hero, enemy, spawn };
// stay, free - passive game_objects (spawn);
// stay, move, wait, attack, dying, free, dead  - actives
enum class obj_state { stay, move, wait, attack, dying, free, dead };

struct position {
    float x = 0.f;
    float y = 0.f;
};
bool operator==(const position &, const position &);
struct characteristics {
    int health = 100;
    int initial_health = 100;
    float attack_speed = 0.5f; // twice per second
    float damage = 0.f;
    float speed = 0.05f;
};
struct enemy_prototype {
    object_model model;
    std::unordered_map<std::string, animation_2d> animations;
    characteristics chars;
};

// GAME_OBJECT
class game_world_map;
class game_object {
protected:
    position pos;
    obj_role role = obj_role::spawn;
    obj_state state = obj_state::stay;
    // default model (using if not animation)
    object_model model;
    // animation
    animation_map animations;
    animation_2d *current_animation = nullptr;
    // timer
    std::unique_ptr<om::timer> timer;
    std::chrono::steady_clock::time_point start_time;
    // todo remove timer from animations

public:
    game_object();
    ~game_object();
    const position &get_position() const;
    const obj_role &get_role() const;
    const obj_state &get_state() const;
    texture_ptr get_texture() const;
    const vao_buffer *get_vao_buffer() const;
    float get_model_width() const;
    float get_freedom_duration() const;
    void set_position(const position &);
    void set_role(const obj_role &);
    void set_model_width(float);
    void set_default_texture(texture_ptr, float);
    void set_up_animations(const animation_map &);
    virtual void act(const obj_state &, bool,
                     const direction &dir = direction::down);
    virtual void update(const game_world_map &);
    virtual void take_damage(int);
};

// GAME_WORLD_MAP
using game_objects_vector = std::vector<game_object *>;
class game_world_map {
    const uint8_t game_object_width = 14; // ~0.10 in OpenGL NDC
    const float game_object_width_ndc;
    static constexpr size_t map_x = 256;
    static constexpr size_t map_y = 144;
    static constexpr size_t map_size = map_x * map_y;
    position exit_pos = {0.f, 0.5f};
    const float step_x;
    const float step_y;
    std::array<std::vector<uint8_t>, map_size> world = {};
    std::map<const direction, std::vector<float>> custom_walls;
    game_objects_vector game_objects;
    void get_current_figure(std::vector<size_t> *, const game_object *) const;
    void fill_world();

public:
    game_world_map();
    float get_map_step_x() const;
    float get_map_step_y() const;
    float get_default_model_width_ndc() const;
    const position &get_exit_pos() const;
    void set_custom_walls(std::string_view, const direction &);
    void set_exit(const position &);
    void update(std::vector<std::unique_ptr<game_object>> *);
    bool is_collision(const game_object *, direction &) const;
    game_object *is_attack(const game_object *, direction &) const;
    bool is_empty_place(const position &) const;
};

// ACTIVE_GAME_OBJECT -> HERO + ENEMY
class active_game_object : public game_object {
    characteristics chars;
    direction dir = direction::down;
    // moving
    position start_pos;
    // using for getting each 0.01 to the collision check
    int checked_way = -1;
    // using for detect collision and stop moving
    std::chrono::steady_clock::time_point start_waiting_time;
    float waiting_time = 0.f;
    // last attack time (for attack speed control)
    std::chrono::steady_clock::time_point last_attack_time;
    // for enemies rising
    int total_damage = 0;

    void stay();
    void move(const direction &d, bool start);
    void attack(bool);
    void die();

public:
    active_game_object();
    explicit active_game_object(const enemy_prototype &);
    ~active_game_object();
    const characteristics &get_characteristics() const;
    int get_health() const;
    float get_health_percent() const;
    void set_characteristics(const characteristics &);
    void set_direction(const direction &);
    virtual void act(const obj_state &, bool,
                     const direction &dir = direction::down) override;
    void update(const game_world_map &) override;
    void update_animation();
    bool is_alive() const;
    void take_damage(int) override;
};
} // namespace game