#pragma once

#include <chrono>

#include "object_model.h"
#include "timer.h"

namespace game {
struct timer_model {
    vao_buffer main_vao;
    vao_buffer process_vao;
    float texture_size = 0.15f;
    texture_ptr day;
    texture_ptr night;
};

class game_timer {
    std::unique_ptr<om::timer> timer;
    timer_model model;

public:
    game_timer();
    ~game_timer();
    const timer_model &get_model() const;
    void set_texture_size(float);
    void set_textures(texture_ptr, texture_ptr); // day, night
    void set_time(const float);
    void update();
    bool is_working();
    bool is_passed(float) const; // in seconds
    void save_check_point();
};
} // namespace game