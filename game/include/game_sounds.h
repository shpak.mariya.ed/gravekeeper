#pragma once

#include <string>
#include <string_view>
#include <unordered_map>

#include "engine.h"

namespace game {
using sound_ptr = om::sound_buffer *;

class game_sounds {
    std::unordered_map<std::string, sound_ptr> sounds;

public:
    void load_from_file(om::engine *, std::string_view);
    sound_ptr get_sound(std::string) const;
};
} // namespace game