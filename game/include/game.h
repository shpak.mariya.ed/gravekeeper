#pragma once

#include <unordered_map>

#include "engine.h"

namespace game {
// GAME
class game {
public:
    virtual ~game();
    virtual bool is_playing() const = 0;
    // setting up
    virtual void set_up_menu(std::string_view) = 0;
    virtual void set_up_game(std::string_view) = 0;
    // set up only prototypes (for using in game process)
    virtual void set_up_enemies(std::string_view) = 0;
    virtual void set_up_background(std::string_view, std::string_view) = 0;
    // 1th - day, 2th - night texture names
    virtual void set_up_timer(std::string_view, std::string_view, float) = 0;
    virtual void create_game_object(std::string_view) = 0;
    virtual void finish_loading() = 0;
    // main loop methods
    virtual void start() = 0;
    virtual void read_event(const om::event &) = 0;
    virtual void update() = 0;
    virtual void render() = 0;
};

game *create_game(om::engine *);
void destroy_game(game *);
} // namespace game