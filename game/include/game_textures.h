#pragma once

#include <string>
#include <string_view>
#include <unordered_map>

#include "engine.h"
#include "object_model.h"

namespace game {
class game_textures {
    std::unordered_map<std::string, texture_ptr> textures;

public:
    void load_from_file(om::engine *, std::string_view);
    texture_ptr get_texture(std::string) const;
};
} // namespace game