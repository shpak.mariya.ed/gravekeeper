#pragma once

#include <iosfwd>
#include <map>
#include <memory>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

#include "matrix.h"

namespace om {
// todo remove turn_off from keys
enum keys { esc, enter, space, up, down, left, right, turn_off };
struct event {
    keys key;
    bool is_pressed;
};

class engine;
engine *create_engine();
void destroy_engine(engine *e);

struct position {
    float x = 0.f;
    float y = 0.f;
};

struct color {
    float r = 0.f;
    float g = 0.f;
    float b = 0.f;
};

struct texture_position {
    float u = 0.f;
    float v = 0.f;
};

struct vertex_full {
    position pos;
    color col;
    texture_position text_pos;
};

struct vertex_colored {
    position pos;
    color col;
};

struct vertex_textured {
    position pos;
    texture_position text_pos;
};

struct triangle_internal {
    virtual void draw() const = 0;
};

struct triangle : private triangle_internal {
    virtual ~triangle();
    friend class engine;

private:
    triangle_internal *get() const;
};

struct triangle_full : public triangle {
    ~triangle_full() override;
    static std::vector<triangle_full> parse_buffer(std::string_view);
    vertex_full v[3];

private:
    void draw() const final;
};

struct triangle_colored : public triangle {
    ~triangle_colored() override;
    static std::vector<triangle_colored> parse_buffer(std::string_view);
    vertex_colored v[3];

private:
    void draw() const final;
};

struct triangle_textured : public triangle {
    ~triangle_textured() override;
    static std::vector<triangle_textured> parse_buffer(std::string_view);
    vertex_textured v[3];

private:
    void draw() const final;
};

std::istream &operator>>(std::istream &, triangle_full &);
std::istream &operator>>(std::istream &, triangle_colored &);
std::istream &operator>>(std::istream &, triangle_textured &);

// TRIANGLE_BUFFER
template <class T> class triangle_buffer {
    std::vector<T> triangles;

public:
    triangle_buffer() {}
    explicit triangle_buffer(const std::vector<T> &buffer)
        : triangles(buffer) {}
    explicit triangle_buffer(std::string_view path)
        : triangles(T::parse_buffer(path)) {}
    const std::vector<T> &buffer() const { return triangles; }
};

// TEXTURE
class texture {
public:
    virtual ~texture();
    virtual void bind() const = 0;
    virtual uint8_t get_id() const = 0;
    virtual uint16_t get_width() const = 0;
    virtual uint16_t get_height() const = 0;
    virtual float get_scale_rate(float) const = 0;
};

// SOUND_BUFFER
class sound_buffer {
public:
    enum class sound_playing_type { once, looped };
    virtual ~sound_buffer();
    virtual void play(const sound_playing_type = sound_playing_type::once) = 0;
    virtual void stop() = 0;
};

// ENGINE
class engine {
protected:
    triangle_internal *get_triangle(const triangle &tr);

public:
    virtual ~engine();
    virtual std::string initialize(std::string_view) = 0;
    virtual float get_time() const = 0;
    virtual uint16_t get_window_width() const = 0;
    virtual uint16_t get_window_height() const = 0;
    virtual float get_window_ratio() const = 0;
    virtual void set_window_width(uint16_t) = 0;
    virtual void set_window_height(uint16_t) = 0;
    virtual bool pool_event(event &e) = 0;

    virtual void create_thread(void (*)()) = 0;
    virtual texture *create_texture(std::string_view) = 0;
    virtual sound_buffer *create_sound_buffer(std::string_view) = 0;
    virtual uint8_t create_shader(std::string_view, std::string_view) = 0;
    virtual void use_shader(uint8_t) = 0;
    virtual void use_default_shader() = 0;
    virtual void set_default_shader(uint8_t) = 0;
    virtual void set_uniform(std::string_view, float) = 0;

    virtual void render(const triangle &) = 0;
    // setting uniform "texture_"
    virtual void render(const triangle &, const texture *) = 0;
    virtual void render(const triangle_buffer<triangle_colored> &) = 0;
    // setting uniform "texture_"
    virtual void render(const triangle_buffer<triangle_textured> &,
                        const texture *) = 0;
    // setting uniform "matrix_", "texture_"
    // todo add other buffer type rendering
    virtual void render(const triangle_buffer<triangle_textured> &,
                        const matrix_2x3 &, const texture *) = 0;
    // morphing: 3th arg should be [-1.0; 1.0]
    virtual void morph(const triangle_colored &, const triangle_colored &,
                       float) = 0;

    virtual void swap_buffers() = 0;
    virtual void terminate() = 0;
};
} // end namespace om