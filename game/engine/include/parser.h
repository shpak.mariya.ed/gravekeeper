#pragma once

#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

namespace om {
// loading file
char *read_file(std::string_view);
// for textures loading
void read_file(const std::string_view, std::vector<std::byte> &);

// parsing
using config_map_t = std::unordered_map<std::string, std::string>;
// the second args are default values
void trim(std::string &);
int str_to_int(std::string);
int str_to_int(std::string, int);
float str_to_float(std::string);
float str_to_float(std::string, float);
std::vector<std::string> get_split_string(std::string, const char);
config_map_t get_config_map_from_string(std::string, const char major_del = ';',
                                        const char minor_del = ':');
config_map_t get_config_map_from_file(std::string_view,
                                      const char major_del = '\n',
                                      const char minor_del = ':');
const std::vector<config_map_t>
get_list_of_config_map_from_file(std::string_view, const char major_del = '\n',
                                 const char minor_del = ':');
} // namespace om