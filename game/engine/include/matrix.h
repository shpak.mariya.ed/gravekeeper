#pragma once

namespace om {
struct vec2 {
    float x = 0.f;
    float y = 0.f;
};

struct matrix_2x3 {
    static matrix_2x3 scale(float);
    static matrix_2x3 scale(vec2);
    static matrix_2x3 move(float);
    static matrix_2x3 move(vec2);
    static matrix_2x3 rotate(float);

    vec2 col0;
    vec2 col1;
    vec2 col2;
};

vec2 operator*(const matrix_2x3 &, const vec2 &);
matrix_2x3 operator*(const matrix_2x3 &, const matrix_2x3 &);
} // namespace om