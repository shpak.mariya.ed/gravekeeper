id: move_up_0
texture: main_hero_move_up
top_left: 0 1
bottom_right: 0.25 0.75
move_by_x: 0.06

id: move_up_1
texture: main_hero_move_up
top_left: 0.25 1
bottom_right: 0.5 0.75
move_by_x: 0.06

id: move_up_2
texture: main_hero_move_up
top_left: 0.5 1
bottom_right: 0.75 0.75
move_by_x: 0.06

id: move_up_3
texture: main_hero_move_up
top_left: 0.75 1
bottom_right: 1 0.75
move_by_x: 0.06

id: move_up_4
texture: main_hero_move_up
top_left: 0 0.75
bottom_right: 0.25 0.5
move_by_x: 0.06

id: move_up_5
texture: main_hero_move_up
top_left: 0.25 0.75
bottom_right: 0.5 0.5
move_by_x: 0.06

id: move_up_6
texture: main_hero_move_up
top_left: 0.5 0.75
bottom_right: 0.75 0.5
move_by_x: 0.06

id: move_up_7
texture: main_hero_move_up
top_left: 0.75 0.75
bottom_right: 1 0.5
move_by_x: 0.06

id: move_up_8
texture: main_hero_move_up
top_left: 0 0.5
bottom_right: 0.25 0.25
move_by_x: 0.06

id: move_up_9
texture: main_hero_move_up
top_left: 0.25 0.5
bottom_right: 0.5 0.25
move_by_x: 0.06

id: move_up_10
texture: main_hero_move_up
top_left: 0.5 0.5
bottom_right: 0.75 0.25
move_by_x: 0.06

id: move_up_11
texture: main_hero_move_up
top_left: 0.75 0.5
bottom_right: 1 0.25
move_by_x: 0.06

id: move_up_12
texture: main_hero_move_up
top_left: 0 0.25
bottom_right: 0.25 0
move_by_x: 0.06