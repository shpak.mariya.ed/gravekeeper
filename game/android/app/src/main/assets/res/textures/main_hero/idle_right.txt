id: idle_right_0
texture: main_hero_idle_right
top_left: 0 1
bottom_right: 0.2 0.75
move_by_x: 0.135

id: idle_right_1
texture: main_hero_idle_right
top_left: 0.2 1
bottom_right: 0.4 0.75
move_by_x: 0.135

id: idle_right_2
texture: main_hero_idle_right
top_left: 0.4 1
bottom_right: 0.6 0.75
move_by_x: 0.135

id: idle_right_3
texture: main_hero_idle_right
top_left: 0.6 1
bottom_right: 0.8 0.75
move_by_x: 0.135

id: idle_right_4
texture: main_hero_idle_right
top_left: 0.8 1
bottom_right: 1 0.75
move_by_x: 0.135

id: idle_right_5
texture: main_hero_idle_right
top_left: 0 0.75
bottom_right: 0.2 0.5
move_by_x: 0.135

id: idle_right_6
texture: main_hero_idle_right
top_left: 0.2 0.75
bottom_right: 0.4 0.5
move_by_x: 0.135

id: idle_right_7
texture: main_hero_idle_right
top_left: 0.4 0.75
bottom_right: 0.6 0.5
move_by_x: 0.135

id: idle_right_8
texture: main_hero_idle_right
top_left: 0.6 0.75
bottom_right: 0.8 0.5
move_by_x: 0.135

id: idle_right_9
texture: main_hero_idle_right
top_left: 0.8 0.75
bottom_right: 1 0.5
move_by_x: 0.135

id: idle_right_10
texture: main_hero_idle_right
top_left: 0 0.5
bottom_right: 0.2 0.25
move_by_x: 0.135

id: idle_right_11
texture: main_hero_idle_right
top_left: 0.2 0.5
bottom_right: 0.4 0.25
move_by_x: 0.135

id: idle_right_12
texture: main_hero_idle_right
top_left: 0.4 0.5
bottom_right: 0.6 0.25
move_by_x: 0.135

id: idle_right_13
texture: main_hero_idle_right
top_left: 0.6 0.5
bottom_right: 0.8 0.25
move_by_x: 0.135

id: idle_right_14
texture: main_hero_idle_right
top_left: 0.8 0.5
bottom_right: 1 0.25
move_by_x: 0.135

id: idle_right_15
texture: main_hero_idle_right
top_left: 0 0.25
bottom_right: 0.2 0
move_by_x: 0.135

id: idle_right_16
texture: main_hero_idle_right
top_left: 0.2 0.25
bottom_right: 0.4 0
move_by_x: 0.135

id: idle_right_17
texture: main_hero_idle_right
top_left: 0.4 0.25
bottom_right: 0.6 0
move_by_x: 0.135

id: idle_right_18
texture: main_hero_idle_right
top_left: 0.6 0.25
bottom_right: 0.8 0
move_by_x: 0.135