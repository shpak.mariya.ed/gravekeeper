id: move_up_0
texture: skeleton_move_up
top_left: 0 1
bottom_right: 0.25 0.75
scale: 0.6

id: move_up_1
texture: skeleton_move_up
top_left: 0.25 1
bottom_right: 0.5 0.75
scale: 0.6

id: move_up_2
texture: skeleton_move_up
top_left: 0.5 1
bottom_right: 0.75 0.75
scale: 0.6

id: move_up_3
texture: skeleton_move_up
top_left: 0.75 1
bottom_right: 1 0.75
scale: 0.6

id: move_up_4
texture: skeleton_move_up
top_left: 0 0.75
bottom_right: 0.25 0.5
scale: 0.6

id: move_up_5
texture: skeleton_move_up
top_left: 0.25 0.75
bottom_right: 0.5 0.5
scale: 0.6

id: move_up_6
texture: skeleton_move_up
top_left: 0.5 0.75
bottom_right: 0.75 0.5
scale: 0.6

id: move_up_7
texture: skeleton_move_up
top_left: 0.75 0.75
bottom_right: 1 0.5
scale: 0.6

id: move_up_8
texture: skeleton_move_up
top_left: 0 0.5
bottom_right: 0.25 0.25
scale: 0.6

id: move_up_9
texture: skeleton_move_up
top_left: 0.25 0.5
bottom_right: 0.5 0.25
scale: 0.6

id: move_up_10
texture: skeleton_move_up
top_left: 0.5 0.5
bottom_right: 0.75 0.25
scale: 0.6

id: move_up_11
texture: skeleton_move_up
top_left: 0.75 0.5
bottom_right: 1 0.25
scale: 0.6

id: move_up_12
texture: skeleton_move_up
top_left: 0 0.25
bottom_right: 0.25 0
scale: 0.6