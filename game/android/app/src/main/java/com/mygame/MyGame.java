package com.mygame;

import org.libsdl.app.SDLActivity;

public class MyGame extends SDLActivity
{
    @Override
    protected String getMainFunction() {
        return "android_main";
    }

    @Override
    protected String[] getLibraries() {
        return new String[]{
                "hidapi",
                "SDL2",
                "game"
        };
    }
}
