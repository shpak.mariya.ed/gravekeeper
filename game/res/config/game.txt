night_duration: 60
day_duration: 10
textures_path: res/config/textures_list.txt
sounds_path: res/config/sounds_list.txt
custom_wall_top_path: res/config/wall_top.txt
custom_wall_bottom_path: res/config/wall_bottom.txt
digits_config_path: res/textures/digits.txt
world_map_exit: -0.065, 0.37