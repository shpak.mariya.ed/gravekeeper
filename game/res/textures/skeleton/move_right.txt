id: move_right_0
texture: skeleton_move_right
top_left: 0 1
bottom_right: 0.2 0.8
scale: 0.7

id: move_right_1
texture: skeleton_move_right
top_left: 0.2 1
bottom_right: 0.4 0.8
scale: 0.7

id: move_right_2
texture: skeleton_move_right
top_left: 0.4 1
bottom_right: 0.6 0.8
scale: 0.7

id: move_right_3
texture: skeleton_move_right
top_left: 0.6 1
bottom_right: 0.8 0.8
scale: 0.7

id: move_right_4
texture: skeleton_move_right
top_left: 0.8 1
bottom_right: 1 0.8
scale: 0.7

id: move_right_5
texture: skeleton_move_right
top_left: 0 0.8
bottom_right: 0.2 0.6
scale: 0.7

id: move_right_6
texture: skeleton_move_right
top_left: 0.2 0.8
bottom_right: 0.4 0.6
scale: 0.7

id: move_right_7
texture: skeleton_move_right
top_left: 0.4 0.8
bottom_right: 0.6 0.6
scale: 0.7

id: move_right_8
texture: skeleton_move_right
top_left: 0.6 0.8
bottom_right: 0.8 0.6
scale: 0.7

id: move_right_9
texture: skeleton_move_right
top_left: 0.8 0.8
bottom_right: 1 0.6
scale: 0.7

id: move_right_10
texture: skeleton_move_right
top_left: 0 0.6
bottom_right: 0.2 0.4
scale: 0.7

id: move_right_11
texture: skeleton_move_right
top_left: 0.2 0.6
bottom_right: 0.4 0.4
scale: 0.7

id: move_right_12
texture: skeleton_move_right
top_left: 0.4 0.6
bottom_right: 0.6 0.4
scale: 0.7

id: move_right_13
texture: skeleton_move_right
top_left: 0.6 0.6
bottom_right: 0.8 0.4
scale: 0.7

id: move_right_14
texture: skeleton_move_right
top_left: 0.8 0.6
bottom_right: 1 0.4
scale: 0.7

id: move_right_15
texture: skeleton_move_right
top_left: 0 0.4
bottom_right: 0.2 0.2
scale: 0.7

id: move_right_16
texture: skeleton_move_right
top_left: 0.2 0.4
bottom_right: 0.4 0.2
scale: 0.7

id: move_right_17
texture: skeleton_move_right
top_left: 0.4 0.4
bottom_right: 0.6 0.2
scale: 0.7

id: move_right_18
texture: skeleton_move_right
top_left: 0.6 0.4
bottom_right: 0.8 0.2
scale: 0.7

id: move_right_19
texture: skeleton_move_right
top_left: 0.8 0.4
bottom_right: 1 0.2
scale: 0.7

id: move_right_20
texture: skeleton_move_right
top_left: 0 0.2
bottom_right: 0.2 0
scale: 0.7

id: move_right_21
texture: skeleton_move_right
top_left: 0.2 0.2
bottom_right: 0.4 0
scale: 0.7

id: move_right_22
texture: skeleton_move_right
top_left: 0.4 0.2
bottom_right: 0.6 0
scale: 0.7

id: move_right_23
texture: skeleton_move_right
top_left: 0.6 0.2
bottom_right: 0.8 0
scale: 0.7