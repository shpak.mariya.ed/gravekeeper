id: move_down_0
texture: skeleton_move_down
top_left: 0 1
bottom_right: 0.25 0.75
scale: 0.57

id: move_down_1
texture: skeleton_move_down
top_left: 0.25 1
bottom_right: 0.5 0.75
scale: 0.57

id: move_down_2
texture: skeleton_move_down
top_left: 0.5 1
bottom_right: 0.75 0.75
scale: 0.57

id: move_down_3
texture: skeleton_move_down
top_left: 0.75 1
bottom_right: 1 0.75
scale: 0.57

id: move_down_4
texture: skeleton_move_down
top_left: 0 0.75
bottom_right: 0.25 0.5
scale: 0.57

id: move_down_5
texture: skeleton_move_down
top_left: 0.25 0.75
bottom_right: 0.5 0.5
scale: 0.57

id: move_down_6
texture: skeleton_move_down
top_left: 0.5 0.75
bottom_right: 0.75 0.5
scale: 0.57

id: move_down_7
texture: skeleton_move_down
top_left: 0.75 0.75
bottom_right: 1 0.5
scale: 0.57

id: move_down_8
texture: skeleton_move_down
top_left: 0 0.5
bottom_right: 0.25 0.25
scale: 0.57

id: move_down_9
texture: skeleton_move_down
top_left: 0.25 0.5
bottom_right: 0.5 0.25
scale: 0.57

id: move_down_10
texture: skeleton_move_down
top_left: 0.5 0.5
bottom_right: 0.75 0.25
scale: 0.57

id: move_down_11
texture: skeleton_move_down
top_left: 0.75 0.5
bottom_right: 1 0.25
scale: 0.57

id: move_down_12
texture: skeleton_move_down
top_left: 0 0.25
bottom_right: 0.25 0
scale: 0.57