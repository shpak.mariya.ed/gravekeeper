#include <cassert>
#include <cmath>
#include <iostream>
#include <stdexcept>

#include "animation_2d.h"
#include "parser.h"

namespace game {
// ANIMATION_2D
animation_2d::animation_2d() = default;
animation_2d::animation_2d(std::string_view path, const game_textures &textures,
                           float width, float window_ratio) {
    std::string dying = "dying";
    if (path.find(dying) != std::string::npos) type = animation_type::ended;
    load_sprites_from_file(path, textures, width, window_ratio);
}
animation_2d::animation_2d(std::vector<sprite> &in) {
    std::copy(in.begin(), in.end(), sprites.begin());
}
void animation_2d::start() { start_time = std::chrono::steady_clock::now(); }
void animation_2d::start_once() {
    start_time = std::chrono::steady_clock::now();
    type = animation_type::ended;
}
const sprite *animation_2d::get_current_sprite() {
    if (sprites.empty()) {
        std::cerr << "No sprites in the animation!" << std::endl;
        return nullptr;
    }
    if (type != animation_type::locked) {
        using namespace std::chrono;
        steady_clock::time_point current_time = steady_clock::now();
        duration<float> pasted_time =
            duration_cast<duration<float>>(current_time - start_time);
        float time_for_each_sprite =
            total_time / static_cast<float>(sprites_count);
        float sprite_num = pasted_time.count() / time_for_each_sprite;
        if (type == animation_type::ended && sprite_num > sprites_count) {
            current_id = static_cast<uint16_t>(sprites_count - 1);
        } else {
            float current_sprite_float =
                fmod(sprite_num, static_cast<float>(sprites_count));
            current_id = static_cast<uint16_t>(current_sprite_float);
        }
    }
    sprite *current_sprite = &(sprites.at(current_id));
    if (current_sprite == nullptr) {
        std::cerr << "The " << current_id << " texture is null!" << std::endl;
        throw std::runtime_error("Error: Cannot get texture!");
    }
    return current_sprite;
}
void animation_2d::load_sprites_from_file(std::string_view path,
                                          const game_textures &textures,
                                          float width, float window_ratio) {
    // getting file source
    std::string file_src = om::read_file(path);
    std::vector<std::string> file_by_lines =
        om::get_split_string(file_src, '\n');
    // filling sprites_map; removing empty lines
    std::vector<std::vector<std::string>> sprites_config;
    std::vector<std::string> sprite_config;
    for (auto line : file_by_lines) {
        if (!line.empty()) sprite_config.push_back(line);
        else {
            sprites_config.push_back(sprite_config);
            sprite_config.clear();
        }
    }
    // todo make filling map and setting up one method
    // filling sprites
    sprites_count = sprites_config.size();
    for (size_t i = 0; i < sprites_count; ++i) {
        std::vector<std::string> *lines = &sprites_config.at(i);
        sprite s;
        for (size_t j = 0; j < lines->size(); ++j) {
            std::string line = lines->at(j);
            std::vector<std::string> config_pair =
                om::get_split_string(line, ':');
            if (config_pair.size() != 2) {
                std::cerr << "Parsing error! Line \"" << line
                          << "\" is incorrect." << std::endl
                          << "Needed format - key: value" << std::endl;
            }
            std::string field_name = (config_pair.at(0));
            std::string field_value = config_pair.at(1);
            om::trim(field_name);
            om::trim(field_value);
            if (field_name == "id") {
                s.id = field_value;
            } else if (field_name == "texture") {
                s.texture = textures.get_texture(field_value);
            } else if (field_name == "top_left") {
                std::vector<std::string> top_left =
                    om::get_split_string(field_value, ' ');
                s.texture_top_left = {om::str_to_float(top_left.at(0)),
                                      om::str_to_float(top_left.at(1))};
            } else if (field_name == "bottom_right") {
                std::vector<std::string> bottom_right =
                    om::get_split_string(field_value, ' ');
                s.texture_bottom_right = {om::str_to_float(bottom_right.at(0)),
                                          om::str_to_float(bottom_right.at(1))};
            } else if (field_name == "scale") {
                s.scale = om::str_to_float(field_value, 1.f);
            } else if (field_name == "move_by_y") {
                s.moving.y = om::str_to_float(field_value, 0.f);
            } else if (field_name == "move_by_x") {
                s.moving.x = om::str_to_float(field_value, 0.f);
            }
        }
        float animation_texture_ratio = s.texture->get_scale_rate(window_ratio);
        float ratio = (s.texture_top_left.v - s.texture_bottom_right.v) /
                      (s.texture_bottom_right.u - s.texture_top_left.u) *
                      animation_texture_ratio;
        s.vao = generate_vao_buffer(width * s.scale, generation_type::to_top,
                                    ratio, s.moving, s.texture_top_left,
                                    s.texture_bottom_right);
        sprites.push_back(s);
    }
    // setting up total_time
    // todo add speed field (in the config file)
    total_time = sprites_count * 0.05f;
    std::size_t is_attack = path.find("attack");
    if (is_attack != std::string::npos) total_time = sprites_count * 0.02f;
}
void animation_2d::set_total_time(float t) { total_time = t; }
void animation_2d::lock_on_one_sprite(float p) {
    type = animation_type::locked;
    if (p < 0.f) p = 0.f;
    else if (p > 1.f)
        p = 1.f;
    current_id =
        static_cast<uint16_t>(static_cast<float>(sprites_count - 1) * p);
}

// others
vao_buffer generate_vao_buffer(float width, const generation_type &gt,
                               float ratio, const om::position &moving,
                               om::texture_position t0,
                               om::texture_position t3) {
    if (width < 0) {
        std::cerr << "Object width is not correct!" << std::endl;
    }
    float height = width * ratio;
    float half_width = width / 2.f;
    float half_height = height / 2.f;
    // square creation
    om::position p0, p1, p2, p3;
    if (gt == generation_type::centered) {
        p0 = {-half_width, half_height};
        p1 = {half_width, half_height};
        p2 = {-half_width, -half_height};
        p3 = {half_width, -half_height};
    } else if (gt == generation_type::to_top) {
        p0 = {-half_width, height};
        p1 = {half_width, height};
        p2 = {-half_width, 0.f};
        p3 = {half_width, 0.f};
    }
    om::texture_position t1, t2;
    t1 = {t3.u, t0.v};
    t2 = {t0.u, t3.v};
    // moving
    float x_offset = width * moving.x;
    float y_offset = height * moving.y;
    p0.x += x_offset;
    p0.y += y_offset;
    p1.x += x_offset;
    p1.y += y_offset;
    p2.x += x_offset;
    p2.y += y_offset;
    p3.x += x_offset;
    p3.y += y_offset;

    om::triangle_textured tr0;
    om::triangle_textured tr1;
    tr0.v[0].pos = p0;
    tr0.v[0].text_pos = t0;
    tr0.v[1].pos = p1;
    tr0.v[1].text_pos = t1;
    tr0.v[2].pos = p2;
    tr0.v[2].text_pos = t2;

    tr1.v[0].pos = p1;
    tr1.v[0].text_pos = t1;
    tr1.v[1].pos = p2;
    tr1.v[1].text_pos = t2;
    tr1.v[2].pos = p3;
    tr1.v[2].text_pos = t3;

    std::vector<om::triangle_textured> buffer;
    buffer.push_back(tr0);
    buffer.push_back(tr1);
    // todo add vertices as a mesh
    vao_buffer vao = vao_buffer(buffer);
    return vao;
}
vao_buffer_colored generate_progress_bar(const om::position &pos,
                                         float model_width, float progress) {
    if (progress <= 0.f) progress = 0.f;
    float bar_width = model_width;
    float bar_height = 0.04;
    float x = pos.x;
    float y = pos.y;
    float offset_x = bar_width / 2.f;
    float left_x = x - offset_x;
    float right_x = x + offset_x;
    float offset_y = 0.3f;
    float bottom_y = y + offset_y;
    float top_y = bottom_y + bar_height;

    /* p0######p1###p2
     * #             #
     * p3######p4###p5
     * |________|
     *  progress    */
    // getting vertices positions
    om::position p0, p1, p2, p3, p4, p5;
    p0 = {left_x, top_y};
    p2 = {right_x, top_y};
    p3 = {left_x, bottom_y};
    p5 = {right_x, bottom_y};
    // progress
    float offset_progress = bar_width * progress;
    p1 = {left_x + offset_progress, top_y};
    p4 = {left_x + offset_progress, bottom_y};
    // getting color
    float r, g;
    if (progress <= 0.5f) {
        r = 1.f;
        g = progress * 2.f;
    } else {
        g = 1.f;
        r = 1.f - (progress - 0.5f) * 2.f;
    }
    om::color progress_color = {r, g, 0.f};
    om::color back_color = {0.5f, 0.5f, 0.5f};
    // creating triangles
    om::triangle_colored tr0, tr1, tr2, tr3;
    tr0.v[0].pos = p0;
    tr0.v[0].col = progress_color;
    tr0.v[1].pos = p1;
    tr0.v[1].col = progress_color;
    tr0.v[2].pos = p3;
    tr0.v[2].col = progress_color;

    tr1.v[0].pos = p1;
    tr1.v[0].col = progress_color;
    tr1.v[1].pos = p3;
    tr1.v[1].col = progress_color;
    tr1.v[2].pos = p4;
    tr1.v[2].col = progress_color;

    tr2.v[0].pos = p1;
    tr2.v[0].col = back_color;
    tr2.v[1].pos = p2;
    tr2.v[1].col = back_color;
    tr2.v[2].pos = p4;
    tr2.v[2].col = back_color;

    tr3.v[0].pos = p2;
    tr3.v[0].col = back_color;
    tr3.v[1].pos = p4;
    tr3.v[1].col = back_color;
    tr3.v[2].pos = p5;
    tr3.v[2].col = back_color;

    std::vector<om::triangle_colored> buffer;
    buffer.push_back(tr0);
    buffer.push_back(tr1);
    buffer.push_back(tr2);
    buffer.push_back(tr3);
    vao_buffer_colored vao = vao_buffer_colored(buffer);
    return vao;
}
} // namespace game