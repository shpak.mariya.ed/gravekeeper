#include <iostream>
#include <stdexcept>

#include "game_textures.h"
#include "parser.h"

namespace game {
void game_textures::load_from_file(om::engine *engine, std::string_view path) {
    om::config_map_t textures_map = om::get_config_map_from_file(path);
    for (auto texture : textures_map) {
        textures.insert(
            std::pair(texture.first, engine->create_texture(texture.second)));
    }
}
texture_ptr game_textures::get_texture(std::string name) const {
    auto it = textures.find(name);
    if (it == textures.end()) {
        std::cerr << "Cannot find texture with the name " << name << "!"
                  << std::endl
                  << "Try to add template into the texture paths file."
                  << std::endl;
        throw std::runtime_error("Error: Cannot set texture up!");
    }
    texture_ptr out = it->second;
    if (out == nullptr) {
        std::cerr << "Texture " << name << " is broken!" << std::endl;
        throw std::runtime_error("Error: Cannot set texture up!");
    }
    return out;
}
} // namespace game