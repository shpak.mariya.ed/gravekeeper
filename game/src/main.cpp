#include <cstdlib>
#include <iostream>
#include <istream>

#include "game.h"

#ifdef __ANDROID__
extern "C" int android_main();
#endif

game::game *game_ptr = nullptr;
void set_up_game() {
    if (game_ptr == nullptr) {
        throw std::runtime_error("Global game pointer is nullptr!");
    }
    std::string_view game_config_path = "res/config/game.txt";
    game_ptr->set_up_game(game_config_path);
    std::string_view enemy_config_path = "res/config/game_object_skeleton.txt";
    game_ptr->set_up_enemies(enemy_config_path);
    game_ptr->set_up_background("background_night", "background_day");
    game_ptr->set_up_timer("day", "night", 0.15f);

    // main_hero initialization
    std::string_view main_hero_config_path =
        "res/config/game_object_main_hero.txt";
    game_ptr->create_game_object(main_hero_config_path);
    // graves initialization
    std::string_view graves_config_path = "res/config/game_object_grave.txt";
    game_ptr->create_game_object(graves_config_path);
    // setting game is ready to be played
    game_ptr->finish_loading();
}

int main() {
    // engine initialization
    std::unique_ptr<om::engine, void (*)(om::engine *)> engine(
        om::create_engine(), om::destroy_engine);

    std::string_view window_config_path = "res/config/window.txt";
    const std::string error = engine->initialize(window_config_path);
    if (!error.empty()) {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    uint8_t shader_id = engine->create_shader("res/shaders/shader.vert",
                                              "res/shaders/shader.frag");
    engine->set_default_shader(shader_id);

    // game initialization
    std::unique_ptr<game::game, void (*)(game::game *)> game(
        game::create_game(engine.get()), game::destroy_game);
    // menu setting up
    std::string_view menu_config_path = "res/config/menu.txt";
    game->set_up_menu(menu_config_path);
    // game start - calling start menu
    game->start();
    // game setting up - in other thread
    game_ptr = game.get();
    engine->create_thread(set_up_game);

    while (game->is_playing()) {
        om::event event;
        while (engine->pool_event(event)) {
            game->read_event(event);
        }
        game->update();
        game->render();
    }

    engine->terminate();

    return EXIT_SUCCESS;
}

#ifdef __ANDROID__
int android_main() { return main(); }
#endif