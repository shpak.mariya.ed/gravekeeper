import argparse
import shutil
from pathlib import Path
from PIL import Image
from tqdm import tqdm


def clear_output_dir(output_dir: Path):
    if output_dir.exists():
        shutil.rmtree(output_dir)

    output_dir.mkdir()


def crop_image(input_path: Path, output_path: Path, x_crop: int, y_crop: int):
    img = Image.open(input_path)

    x_size = img.size[0]
    y_size = img.size[1]
    assert x_size > 2 * x_crop
    assert y_size > 2 * y_crop

    left = x_crop
    top = y_crop
    right = x_size - x_crop
    bottom = y_size - y_crop

    cropped_img = img.crop([left, top, right, bottom])
    cropped_img.save(output_path)


def parse_args():
    parser = argparse.ArgumentParser(
        description="Tool for cropping PNG images in batch."
    )
    parser.add_argument(
        "--input_dir", type=Path, required=True, help="Input dir with images"
    )
    parser.add_argument(
        "--output_dir",
        type=Path,
        default=Path("./crops_out"),
        help="Output dir for cropped images. Will be recreated each time.",
    )
    parser.add_argument(
        "--x_crop",
        type=int,
        default=150,
        help="How much pixels will be cropped from right and left.",
    )
    parser.add_argument(
        "--y_crop",
        type=int,
        default=150,
        help="How much pixels will be cropped from top and bottom.",
    )

    return parser.parse_args()


def main():
    """
    Tool for cropping PNG images in batch.

    Usage example:
        python png_cropper.py --input_dir=./from --output_dir=./to --x_crop=150 --y_crop=150
    """
    args = parse_args()

    clear_output_dir(args.output_dir)

    for img_path in tqdm(list(args.input_dir.glob("**/*.png"))):
        crop_image(img_path, args.output_dir / img_path.name, args.x_crop, args.y_crop)


if __name__ == "__main__":
    main()
