import argparse
from datetime import datetime
from pathlib import Path
from typing import List, Tuple

import numpy
from PIL import Image


def tprint(message: str):
    ts = datetime.strftime(datetime.now(), "%H:%M:%S.%f")
    message = f"{ts} {message}"
    print(message)


def log_timing(f):
    def wrapper(*args, **kwargs):
        start_dt = datetime.now()
        tprint(f"Start {f.__name__}")
        res = f(*args, **kwargs)
        tprint(f"Finish {f.__name__}. Duration={datetime.now() - start_dt}")

        return res

    return wrapper


def select_border(image_path: Path, output_path: Path):
    img = Image.open(image_path)
    alpha_img = img.getchannel("A")
    bm = numpy.array(alpha_img)

    points = extract_border_points(bm)

    serialize_points(points, output_path)


@log_timing
def extract_border_points(bm):
    border_points = []
    y_size = bm.shape[0]
    x_size = bm.shape[1]
    for x in range(bm.shape[1]):
        for y in range(bm.shape[0] - 1):
            current_pixel = bm[y][x]
            pixel_below = bm[y + 1][x]
            if current_pixel != pixel_below:
                border_points.append((x / x_size, y / y_size))
                break
    return border_points


def serialize_points(points: List[Tuple[float, float]], output_path: Path):
    with output_path.open("w") as f:
        f.writelines([f"{point[0]} {point[1]}\n" for point in points])


def parse_args():
    parser = argparse.ArgumentParser(description="Select border for image")
    parser.add_argument("--image_path", type=Path, required=True, help="Path of image")
    parser.add_argument(
        "--output_path",
        type=Path,
        default=Path("./border"),
        help="Output file with border",
    )

    return parser.parse_args()


def main():
    """
    Select border for image.

    Usage example:
        python border_selector.py --image_path=./my_img.png --output_path=./my_border
    """
    args = parse_args()
    select_border(args.image_path, args.output_path)


if __name__ == "__main__":
    main()